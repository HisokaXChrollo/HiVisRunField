using Toybox.Application as App;
using Toybox.WatchUi as Ui;
using Toybox.Graphics as Graphics;
using Toybox.System as System;
using Toybox.UserProfile as UserProfile;
using Toybox.Time as Time;

//! @author Brian Bannister, first version of this started from BikersFields by Konrad Paumann
class HiVisRunField2 extends App.AppBase {
    hidden var view;

    function initialize() {
        App.AppBase.initialize();
    }

    function getInitialView() {
        view = new HiVisRunView();
        onSettingsChanged();
        return [ view ];
    }


    function onSettingsChanged() {

    }

    function onStop(state) {
        view.onStop(state);
    }

}

//! A DataField deisgned to fill the entire screen
//! @author Brian Bannister
class HiVisRunView extends Ui.DataField {

    hidden var SCREEN_SIZE = 260;
    hidden var LARGE_FONT = Graphics.FONT_SYSTEM_NUMBER_THAI_HOT;
    hidden var MEDIUM_FONT = Graphics.FONT_SYSTEM_NUMBER_MEDIUM;
    hidden var LABEL_FONT = Graphics.FONT_SYSTEM_XTINY;
    
    hidden var BIG_VERTICAL_DOWNSET = 0;
    hidden var BIG_INSET = 0;
    hidden var SMALL_INSET = 0;
    hidden var SMALL_VERTICAL_DOWNSET = 0;
    hidden var SMALL_SECOND_VERTICAL_DOWNSET = 0;
    hidden var BIG_SECOND_VERTICAL_DOWNSET = 0;
    hidden var BIG_LABEL_DOWNSET = 0;
    
    hidden var SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 0;
    hidden var BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 0;
    hidden var SMALL_BOUNDING_BOX_RELATIVE_DROP = 0;
    hidden var BIG_BOUNDING_BOX_RELATIVE_DROP = 0;
    
    hidden var LABEL_FONT_SPACING = 0;
    hidden var SMALL_LABEL_FONT_DOWNSET = 0;
    
    hidden var SHORT_LABEL_NUM_LETTERS = 3;
    hidden var LONG_LABEL_NUM_LETTERS = 4;
    
    hidden var SUFFER_TEXT_DOWNSET = 4;
    hidden var SUFFER_BOX_HEIGHT = 23;

	hidden var scrollProgress = 0;

    hidden var kmOrMileInMeters = 1000;
    hidden var textColor = Graphics.COLOR_BLACK;
    hidden var backgroundColor = Graphics.COLOR_WHITE;
    hidden var headerColor = Graphics.COLOR_DK_GRAY;

    hidden var hrZone0 = 114;
    hidden var hrZone1 = 126;
    hidden var hrZone2 = 138;
    hidden var hrZone3 = 150;
    hidden var hrZone4 = 165;
    hidden var hrZone5 = 175;

    hidden var currentHR = 0;
    hidden var currentZone = 0;
    hidden var averageHR = 0;

    hidden var mafHR = 0;
    hidden var hasMaf = false;
    hidden var mafLowerBound = 0;
    hidden var mafMiddleBound = 0;
    hidden var mafHigherBound = 0;
    
    hidden var timeInMaf = 0;
    hidden var timeOverMaf = 0;

    hidden var currentPace = 0;
    hidden var averagePace = 0;

    hidden var distance = 0;
    hidden var elapsedTime = 0;
    hidden var trainingEffect = 0;

    hidden var ascent = 0.0;

    //Suffer Score Values
    //Activities with a Suffer Score of 100 to 150 will be classified as "tough;" Green
    //those with a Suffer Score of 151 to 250 are "Extreme;" Yellow
    //and those with a suffer Suffer Score above 250 are "Epic." Red
    hidden var sufferScore = 0.0;
    hidden var lastMeasuredSufferTime = 0;
    
    hidden enum {
    	F245,
    	F245M,
    	F255, 
    	F255M,
    	F255S,
    	F255SM,
    	F6Pro,
        F6XPro,
        F645,
        F645M,
        F6,
        F945,
        F945LTE,
        F5XPlus,
        F6SPro,
        F6S,
        F6C,
        F7,
        F7S,
        F7X,
        F5X,
        F5SPlus,
        F5S,
        F5Plus,
        F5,
        F745,
        F935,
        Venu2,
        Venu2S,
        Venu,
        VenuMB,
        Vivo4,
        Vivo4S
    }
    hidden var watchType = F6Pro;
    


    function initialize() {
        DataField.initialize();
        var profile = UserProfile.getProfile();
        var sport = UserProfile.getCurrentSport();
        if( profile != null && sport != null) {
            var heartRateZones = UserProfile.getHeartRateZones(sport);
            if (null != heartRateZones) {
                if (null != heartRateZones[0]) {
                    hrZone0 = heartRateZones[0];
                }
                if (null != heartRateZones[1]) {
                    hrZone1 = heartRateZones[1];
                }
                if (null != heartRateZones[2]) {
                    hrZone2 = heartRateZones[2];
                }
                if (null != heartRateZones[3]) {
                    hrZone3 = heartRateZones[3];
                }
                if (null != heartRateZones[4]) {
                    hrZone4 = heartRateZones[4];
                }
                if (null != heartRateZones[5]) {
                    hrZone5 = heartRateZones[5];
                }
            }
            var year = 1970 + Time.now().value() / Time.Gregorian.SECONDS_PER_YEAR;
            mafHR = 180 - (year  - profile.birthYear);
            var useStrictMaf = getPropertyById("Strict_MAF_prop");
            if (useStrictMaf) {
	            mafLowerBound = mafHR - 11;
	            mafMiddleBound = mafHR - 6;
	            mafHigherBound = mafHR + 2;
            }
            else {
            	mafLowerBound = getPropertyById("Low_MAF_prop");
            	mafHigherBound = getPropertyById("High_MAF_prop");
            	if (mafHR > mafLowerBound && mafHR < mafHigherBound) {
            		mafMiddleBound = mafHR;
            	}
            	else {
            		mafMiddleBound = mafLowerBound + (mafHigherBound - mafLowerBound) / 2;
            	}
            }
        }
        
        SCREEN_SIZE = System.getDeviceSettings().screenWidth;
        setWatchType();
        setWatchLayout(watchType);
        
        //System.println("screenWidth = " + SCREEN_SIZE);
        //System.println("screenHeight = " + System.getDeviceSettings().screenHeight);
        //printWatchType();
        
    }


    function onStop(state) {
		
    }

    //! The given info object contains all the current workout
    function compute(info) {
        
        if (info.currentSpeed != null) {
            currentPace = (.6 * currentPace) + (.4 * getPaceFromSpeed(info.currentSpeed));
        }
        if (null != info.averageSpeed) {
            averagePace = getPaceFromSpeed(info.averageSpeed);
        }

        currentHR = resultOrZero(info.currentHeartRate);
        averageHR = resultOrZero(info.averageHeartRate);
		
        // these two must be called before elapsedTime is updated and after HR is updated.
        setTimeInMaf(info.timerTime);
        setTimeOverMaf(info.timerTime);

        elapsedTime = resultOrZero(info.timerTime);
        distance = resultOrZero(info.elapsedDistance);
        trainingEffect = resultOrZero(info.trainingEffect);

        currentZone = getHRZone(currentHR);

        ascent = resultOrZero(info.totalAscent);

        setSufferScore();
        
    }


    function onLayout(dc) {
        setMetricOrImperial();
        onUpdate(dc);
    }

    function onUpdate(dc) {
        setColors();
        // reset background
        var color = getBackgroundColor();
        dc.setColor(color, color);
        dc.fillRectangle(0, 0, SCREEN_SIZE, SCREEN_SIZE);

        drawValues(dc);
    }

	// updating in v5 to show main fields at top and bottom, and smaller fields in a cluster in the middle
    function drawValues(dc) {
        // during layout recheck that we need a MAF colour for the screen background
        hasMaf = false;
   
		//drawField(dc, x, y, width, font, fieldSelection, leftAligned)
        drawField(dc, SMALL_INSET, SMALL_VERTICAL_DOWNSET, (SCREEN_SIZE/2)-SMALL_INSET, MEDIUM_FONT, getTopMiddleLeftField(), true);
        drawField(dc, (SCREEN_SIZE/2), SMALL_VERTICAL_DOWNSET, (SCREEN_SIZE/2)-SMALL_INSET, MEDIUM_FONT, getTopMiddleRightField(), false);
        
        drawField(dc, BIG_INSET, BIG_VERTICAL_DOWNSET, SCREEN_SIZE-BIG_INSET, LARGE_FONT, getTopField(), true);
        drawField(dc, BIG_INSET, BIG_SECOND_VERTICAL_DOWNSET, SCREEN_SIZE-BIG_INSET, LARGE_FONT, getBottomField(), true);
        
        drawField(dc, SMALL_INSET, SMALL_SECOND_VERTICAL_DOWNSET, (SCREEN_SIZE/2)-SMALL_INSET, MEDIUM_FONT, getBottomMiddleLeftField(), true);
        drawField(dc, (SCREEN_SIZE/2), SMALL_SECOND_VERTICAL_DOWNSET, (SCREEN_SIZE/2)-SMALL_INSET, MEDIUM_FONT, getBottomMiddleRightField(), false);

        showTrainingEffect(dc);
        showSufferScore(dc);

        if (sufferScore <= 0) {
        		scrollText(dc, "Use Garmin Express to choose which datafields are shown below...");
        }
        
        //dc.drawLine((SCREEN_SIZE/2), 0, (SCREEN_SIZE/2), SCREEN_SIZE);

    }


    function drawField(dc, x, y, width, font, fieldSelection, leftAligned) {   
    
        var fillColor = getColor(fieldSelection);          
    	var inkColor = getInkForBackground(fillColor);
    	
    	var textHeight = 0;
        var fillY = y;
    	
        if (LARGE_FONT == font) {
          	fillY += BIG_BOUNDING_BOX_RELATIVE_HEIGHT;
          	textHeight = BIG_BOUNDING_BOX_RELATIVE_DROP;
		}
		else {
			fillY += SMALL_BOUNDING_BOX_RELATIVE_HEIGHT;
          	textHeight = SMALL_BOUNDING_BOX_RELATIVE_DROP;
          	// draw a line between the fields
			dc.setColor(inkColor, Graphics.COLOR_TRANSPARENT);
        	dc.drawLine(SCREEN_SIZE / 2, fillY, SCREEN_SIZE / 2, fillY + textHeight);
		}
		
		if (null != fillColor && Graphics.COLOR_TRANSPARENT != fillColor) {
            dc.setColor(fillColor, Graphics.COLOR_TRANSPARENT);
            dc.fillRectangle(x, fillY, width, textHeight);
        }
        


        var text = getValue(fieldSelection);
        var alignment = leftAligned ? Graphics.TEXT_JUSTIFY_LEFT : Graphics.TEXT_JUSTIFY_RIGHT;
        // checks if the text can fit, if not then removes the label, then reduces the spacing between characters, 
        // and if still too long deletes characters from the end
    	var predictedWidth = dc.getTextWidthInPixels(text, font);
    	var actualWidth = predictedWidth;
    	
    	// if everything's going to fit then leave space for the label
        if (predictedWidth <= width - LABEL_FONT_SPACING) {       	
        	var textX = leftAligned ? x + LABEL_FONT_SPACING : x + width - LABEL_FONT_SPACING;
        	dc.setColor(inkColor, Graphics.COLOR_TRANSPARENT);
        	dc.drawText(textX, y, font, text, alignment);
        }
        else {
        	// otherwise use the label space and see if that will fit
        	if (predictedWidth <= width) {
        		//System.println("2-" + text);        		
        		dc.setColor(inkColor, Graphics.COLOR_TRANSPARENT);
	        	dc.drawText(x, y, font, text, Graphics.TEXT_JUSTIFY_LEFT);

	        }
	        //if it still won't fit then condense the text
	        else {
        		//System.println("3-" + text);
        		if (font != LARGE_FONT) { 
		        	// work out how wide the condensed version is an draw it nearer the middle of the 
		        	// screen if possible
			        actualWidth = condense(dc, x, y, width, font, text, true);
			        if (actualWidth < width) {
			        	if (x < SCREEN_SIZE / 2) {
			        		x = (SCREEN_SIZE / 2) - actualWidth - 4;
		        		}
		        		else {
		        			x = (SCREEN_SIZE / 2) + 4;
		        		}
			        }		        
		        }
		        dc.setColor(inkColor, Graphics.COLOR_TRANSPARENT);
		        condense(dc, x, y, width, font, text, false);
		        x += (leftAligned? -3: 3);
	        }
        }
        // draw the label in the best place left
        drawLabel(dc, x, fillY, max(actualWidth, width), font, fieldSelection, leftAligned);

        //return textWidth;
        //return width;
    }
    
    function condense(dc, x, y, width, font, text, justCalculateWidth) {
    	var cursor = 0;
        if (null != text && "" != text) {
        	var full = false;
            for (var i = 0; i < text.length(); i++) {
                var letter = text.substring(i, i+1);
                var adjustment = -4;
                var drawX = x + cursor;
                if (":".equals(letter) || ".".equals(letter) || ",".equals(letter)) {
                    adjustment = -1;
                    drawX += 1;
                }
                else {
                	if ("1".equals(letter)) {
                		adjustment = -6;
                	}
                }
                var letterWidth = dc.getTextWidthInPixels(letter, font) + adjustment;
                if (font == LARGE_FONT || cursor + letterWidth <= width) {
                	cursor += letterWidth;
                	if (!justCalculateWidth) {
                		dc.drawText(drawX, y, font, letter, Graphics.TEXT_JUSTIFY_LEFT);
                	}
            	}
            	else {
            		full = true;
            	}
            }
        }
        return cursor;
    }
    
    function drawLabel(dc, x, y, width, font, fieldSelection, leftAligned) {
    	if(!leftAligned) {
    		x += width;
		}
		var isSmallFont = (LARGE_FONT != font);
		if (isSmallFont) {
			y += SMALL_LABEL_FONT_DOWNSET;
		}
		else {
			y += BIG_LABEL_DOWNSET;
		}
        var label = getLabel(fieldSelection, isSmallFont);
        dc.setColor(headerColor, Graphics.COLOR_TRANSPARENT);
        drawVerticalText(dc, x, y, LABEL_FONT, label);
        //System.println("label font width: "  + dc.getTextWidthInPixels("w", LABEL_FONT));
    }


    function setSufferScore() {
        var timeInZone = (elapsedTime - lastMeasuredSufferTime) / 3600000.00;
        lastMeasuredSufferTime = elapsedTime;

        if (1 == currentZone) {
            sufferScore += 12.0 * timeInZone;
        }
        else if (2 == currentZone) {
            sufferScore += 24.0 * timeInZone;
        }
        else if (3 == currentZone) {
            sufferScore += 45.0 * timeInZone;
        }
        else if (4 == currentZone) {
            sufferScore += 100.0 * timeInZone;
        }
        else if (5 == currentZone || 6 == currentZone) {
            sufferScore += 120.0 * timeInZone;
        }
    }
    
    function setTimeInMaf(newElapsedTime) {
    	if ((mafLowerBound <= currentHR) && (currentHR <= mafHigherBound)) {
    		timeInMaf += newElapsedTime - elapsedTime;
    	}
    }
        
    function setTimeOverMaf(newElapsedTime) {
    	if (currentHR > mafHigherBound) {
    		timeOverMaf += newElapsedTime - elapsedTime;
    	}
    }

    function showTrainingEffect(dc) {
        if (null != trainingEffect && 0 != trainingEffect) {
            var message, fillColor;
            var color = textColor;
            if (2 > trainingEffect) {
                message = "EASY RUN";
                fillColor = Graphics.COLOR_WHITE;
            }
            else if (trainingEffect < 3) {
                message = "MAINTAIN FITNESS";
                fillColor = Graphics.COLOR_GREEN;
            }
            else if (trainingEffect < 4) {
                message = "IMPROVE FITNESS";
                fillColor = Graphics.COLOR_YELLOW;
            }
            else if (trainingEffect < 5) {
                message = "LOTS FITTER";
                fillColor = Graphics.COLOR_BLUE;
            }
            else {
                message = "OVERREACHING";
                fillColor = Graphics.COLOR_RED;
            }

            dc.setColor(fillColor, fillColor);
            dc.fillRectangle(0, 236*SCREEN_SIZE/260, SCREEN_SIZE, 26*SCREEN_SIZE/260);

            dc.setColor(color, Graphics.COLOR_TRANSPARENT);
            dc.drawText(SCREEN_SIZE/2, 235*SCREEN_SIZE/260, LABEL_FONT, message, Graphics.TEXT_JUSTIFY_CENTER);
        }
    }

    //Suffer Score Values
    //Activities with a Suffer Score of 100 to 150 will be classified as "tough;" Green
    //those with a Suffer Score of 151 to 250 are "Extreme;" Yellow
    //and those with a suffer Suffer Score above 250 are "Epic." Red
    function showSufferScore(dc) {
//        sufferScore = 251;

        var color = Graphics.COLOR_WHITE;
        if (100 <= sufferScore) {
            if (sufferScore <= 150) {
                color = Graphics.COLOR_GREEN;
            }
            else if (sufferScore <= 250) {
                color = Graphics.COLOR_YELLOW;
            }
            else {
                color = Graphics.COLOR_RED;
            }
        }

        dc.setColor(color, Graphics.COLOR_TRANSPARENT);
        dc.fillRectangle(0, 0, SCREEN_SIZE, SUFFER_BOX_HEIGHT);

        dc.setColor(textColor, Graphics.COLOR_TRANSPARENT);
        dc.drawText(SCREEN_SIZE/2, SUFFER_TEXT_DOWNSET, LABEL_FONT, "Suffer: " + formatAsInt(sufferScore), Graphics.TEXT_JUSTIFY_CENTER);
    }

    function getHR() {
        if (currentHR == null) {
            currentHR = 0;
        }
        return currentHR;
    }

    function getAverageHR() {
        if (averageHR == null) {
            averageHR = 0;
        }
        return averageHR;
    }

    function getHRZone(hr) {
        var result;

        if (null == hr || hr < hrZone0) {
            result = 0;
        }
        else if (hr < hrZone1) { // 126
            result = 1;
        }
        else if (hr < hrZone2) { // 138
            result = 2;
        }
        else if (hr < hrZone3) { // 150
            result = 3;
        }
        else if (hr < hrZone4) { // 163
            result = 4;
        }
        else if (hr < hrZone5) { // 175
            result = 5;
        }
        // above zone 5!
        else {
            result = 6;
        }

        return result;
    }

    function getHRColor(hr) {
        var color;
        var zone = null == hr ? currentZone : getHRZone(hr);

        if (null == zone) {
            zone = 0;
        }

        if (2 > zone) { // 126
            color = Graphics.COLOR_WHITE;
        }
        else if (2 == zone) { // 138
            color = Graphics.COLOR_GREEN;
        }
        else if (3 == zone) { // 150
            color = Graphics.COLOR_YELLOW;
        }
        else if (4 == zone) { // 163
            color = Graphics.COLOR_ORANGE;
        }
        else if (5 == zone) { // 175
            color = Graphics.COLOR_RED;
        }
        // above zone 5!
        else {
            color = Graphics.COLOR_PINK;
        }
        return color;
    }

    function getMAFColor() {
        hasMaf = true;
        var color = backgroundColor;

        if (currentHR > mafHigherBound) {
            color = Graphics.COLOR_RED;
        }
        else if (currentHR > mafMiddleBound) {
            color = Graphics.COLOR_GREEN;
        }
        else if (currentHR > mafLowerBound) {
            color = Graphics.COLOR_BLUE;
        }
        return color;
    }

    function getBackgroundColor() {
        var result = backgroundColor;
        if (hasMaf) {
            result = getMAFColor();
        }
        return result;
    }

    function getDistance() {
        var distStr = "0.00";
        if (distance > 0) {
            var distanceKmOrMiles = (distance / kmOrMileInMeters);

            //distanceKmOrMiles += 10;

           if (100 > distanceKmOrMiles) {
                distStr = distanceKmOrMiles.format("%.2f");
            }
            else {
                distStr = distanceKmOrMiles.format("%.1f");
            }
        }
        return distStr;
    }

    function getClock() {
        var clockTime = System.getClockTime();

        var minute = clockTime.min < 10 ? "0" + clockTime.min.format("%.1d") : clockTime.min.format("%.2d");

        return Lang.format("$1$:$2$", [computeHour(clockTime.hour), minute]);

    }


    function getPace() {
        return 100 > currentPace ? minutesToTime(currentPace) : "-:--";
    }

    function getAveragePace() {
        return 100 > averagePace ? minutesToTime(averagePace) : "-:--";
    }

    function getAscent() {
        var result = 0.0;
        if (1000 != kmOrMileInMeters) {
            result = ascent * 3.2808;
        }
        else {
            result = ascent;
        }
//        result = 99.4;
        return result;
    }


    function getDuration() {
        var result;
        if (elapsedTime != null && elapsedTime > 0) {
            var minutes = elapsedTime / 1000.0 / 60.0;
            result = minutesToTime(minutes);// + 1000);
        } else {
            result = "0:00";
        }
        return result;
    }
    
    function getPercentageMaf() {
    	var percentageMaf = 0.0;
    	if (timeInMaf != null && timeInMaf > 0) {
    		percentageMaf = (0.0 + timeInMaf) / (0.0 + elapsedTime);
		}
    	// System.println("%MAF = " + percentageMaf);
    	return "" + formatAsInt(percentageMaf * 100) + "%";
    	//return "100%";
    }
    
    function getTimeOverMaf() {
    	var result;
    	if (timeOverMaf != null && timeOverMaf > 0) {
            var minutes = timeOverMaf / 1000.0 / 60.0;
            result = minutesToTime(minutes);
        } else {
            result = "0:00";
        }
        return result;
        //return "10:00";
    }

    function getLabel(fieldSelection, short) {
//        fieldSelection = 3;
		var digits = short ? SHORT_LABEL_NUM_LETTERS : LONG_LABEL_NUM_LETTERS;

        var result = "";
        if (0 == fieldSelection) {
            result = "dist".substring(0, digits);
        }
        else if (1 == fieldSelection) {
            result = "time".substring(0, digits);
        }
        else if (2 == fieldSelection) {
            result = "pace".substring(0, digits);
        }
        else if (3 == fieldSelection) {
            result = "avpc".substring(0, digits);
        }
        else if (4 == fieldSelection) {
            result = "clock".substring(0, digits);
        }
        else if (5 == fieldSelection) {
            result = "hr";
        }
        else if (6 == fieldSelection) {
            result = 2 == digits  ? "ah" : "ahr";
        }
        else if (7 == fieldSelection) {
            result = "ascnt".substring(0, digits);
        }
        else if (8 == fieldSelection) {
            result =  2 == digits ?  "mf" : "maf";
        }
        else if (9 == fieldSelection) {
            result = "%maf".substring(0, digits);
        }
        else if (10 == fieldSelection) {
            result = "omaf".substring(0, digits);
        }
        return result;
    }

    function getValue(fieldSelection) {
        var result = "";
        if (0 == fieldSelection) {
            result = getDistance();
            //result = "35.34";
        }
        else if (1 == fieldSelection) {
            result = getDuration();
            //result = "9:29:34";
        }
        else if (2 == fieldSelection) {
            result = getPace();
            //result = "19:59";
        }
        else if (3 == fieldSelection) {
            result = getAveragePace();
        }
        else if (4 == fieldSelection) {
            result = getClock();
            //result ="12:55";
        }
        else if (5 == fieldSelection || 8 == fieldSelection) {
            result = formatAsInt(getHR());
            //result = "999";
        }
        else if (6 == fieldSelection) {
            result = formatAsInt(getAverageHR());
        }
        else if (7 == fieldSelection) {
            result = formatAsInt(getAscent());
            //result = "10999";
        }
        else if (9 == fieldSelection) {
            result = getPercentageMaf();
            //result = "100%";
        }
        else if (10 == fieldSelection) {
            result = getTimeOverMaf();
            //result = "15:59:23";
        }
        return result;
    }

    function getColor(fieldSelection) {
        var result = Graphics.COLOR_TRANSPARENT;
        if (5 == fieldSelection) {
            result = getHRColor(null);
        }
        else if (6 == fieldSelection) {
            result = getHRColor(getAverageHR());
        }
        else if (8 == fieldSelection) {
            result = getMAFColor();
        }
        return result;
    }

    function getInkForBackground(fillColor) {
        var result = textColor;
        if (0xFF0000 == fillColor) {
            result = Graphics.COLOR_WHITE;
        }
        else if (textColor == fillColor) {
            result = backgroundColor;
        }
        return result;
    }

    function getTopField() {
        var result = getPropertyById("TOP_prop");
        return result;
    }

    function getTopMiddleLeftField() {
        var result = getPropertyById("TML_prop");
        return result;
    }

    function getTopMiddleRightField() {
        var result = getPropertyById("TMR_prop");
//        result = 7;
        return result;
    }

    function getBottomField() {
        return getPropertyById("BOTTOM_prop");
    }

    function getBottomMiddleLeftField() {
        return getPropertyById("BML_prop");
    }


    function getBottomMiddleRightField() {
        return getPropertyById("BMR_prop");
    }

    function getPaceFromSpeed(speed) {
        return 0 == speed ? 0 : kmOrMileInMeters/(60 * speed);
    }

    function computeHour(hour) {
        if (hour < 1) {
            return hour + 12;
        }
        if (hour >  12) {
            return hour - 12;
        }
        return hour;
    }

    function minutesToTime(minutes) {
        var wholeHours = (minutes/60).toNumber();
        var wholeMinutes = (minutes - (wholeHours * 60)).toNumber();
        var seconds = (60 * (minutes - wholeMinutes - (wholeHours * 60))).toNumber();

        var result = "";
        if (wholeHours > 0) {
            result += wholeHours + ":" + wholeMinutes.format("%02d");
        }
        else {
            result += wholeMinutes;
        }
        result += ":" + seconds.format("%02d");
        return result;

    }

    function drawVerticalText(dc, x, y, font, text) {
        var height = dc.getFontAscent(font)*0.9;
        for (var i = 0; i < text.length(); i++) {
            var letter = text.substring(i, i+1);
            dc.drawText( x, y + (i * height), font, letter, Graphics.TEXT_JUSTIFY_CENTER);
        }
    }


    function formatAsInt(data) {
        var result = "0";
        if (null != data) {
            result = data.format("%.0f");
        }
        return result;
    }

    function resultOrZero(field) {
        return null != field ? field : 0;
    }

    function setColors() {
        var colorSetting = getBackgroundColor();
        if (null != colorSetting && colorSetting != backgroundColor) {
            backgroundColor = colorSetting;
            textColor = (backgroundColor == Graphics.COLOR_BLACK) ? Graphics.COLOR_WHITE : Graphics.COLOR_BLACK;
            headerColor = (backgroundColor == Graphics.COLOR_BLACK) ? Graphics.COLOR_LT_GRAY: Graphics.COLOR_DK_GRAY;
        }
    }

    function setMetricOrImperial() {
        if (System.getDeviceSettings().distanceUnits == System.UNIT_METRIC) {
            kmOrMileInMeters = 1000;
        } else {
            kmOrMileInMeters = 1610;
        }
    }

    function getPropertyById(id) {
        var result = App.getApp().getProperty(id);
        if (null == result) {
            result = 0;
        }
        return result;
    }

    function max(left, right) {
        return left > right ? left : right;
    }

    function min(left, right) {
        return left > right ? right : left;
    }


    function scrollText(dc, text) {
    	var x = SCREEN_SIZE - scrollProgress;
    	var textWidth = dc.getTextWidthInPixels(text, LABEL_FONT) + 10;
    	dc.setColor(Graphics.COLOR_YELLOW, Graphics.COLOR_YELLOW);
        dc.fillRectangle(x-2, 0, textWidth, SUFFER_BOX_HEIGHT);

        dc.setColor(textColor, Graphics.COLOR_TRANSPARENT);
        dc.drawText(x, SUFFER_TEXT_DOWNSET, LABEL_FONT, text, Graphics.TEXT_JUSTIFY_LEFT);

        scrollProgress += 20;
        if (scrollProgress >= SCREEN_SIZE + textWidth) {
        	scrollProgress = 0;
        }
    }
    
        
    function setWatchType() {
    	//6pro 006-B3290-00
        //6xpro 006-B3291-00
        //245 006-B3076-00
        //245M 006-B3077-00
        //255 006-B3992-00, 3992
        //255M 006-B3990-00, 3990, 260
        //255S 006-B3993-00, 3993, 218
        //255SM 006-B3991-00, 3991, 218
        //645 006-B2886-00
        //645M 006-B2888-00
        //6 006-B3289-00
        //945 006-B3113-00
        //945LTE 006-B3652-00
        //5XPlus 006-B3111-00
        //6SPro 006-B3288-00
        //6S 006-B3287-00
        //6Chronos 006-B2432-00
        //7 006-B3906-00, 3906, 260
        //7S 006-B3905-00, 3905, 240
        //7X 006-B3907-00, 3907, 280
        //5X 006-B2604-00
        //5SPlus 006-B2900-00
        //5S 006-B2544-00
        //5Plus 006-B3110-00
        //5 006-B2697-00
        //745 006-B3589-00
        //935 006-B2691-00
        //Venu2 006-B3703-00
        //Venu2S 006-B3704-00
        //Venu 006-B3226-00
        //VenuMB 006-B3740-00
        //Vivoactive4 006-B3225-00
        //Vivoactive4S 006-B3224-00
        var partNumber = "";
        var id = System.getDeviceSettings().partNumber;
        if (null != id && 9 < id.length()) {
        	partNumber = id.substring(5,9);
        } 
        /*
        System.println("id: " + id);
        System.println("partNumber: " + partNumber);
        System.println("SCREEN_SIZE: " + SCREEN_SIZE);
        */
            
        if (218 == SCREEN_SIZE) {
        	switch (partNumber) {
	        	case "2432":
	        		watchType = F6C;
	        		break;
	        	case "2544":
	        		watchType = F5S;
	        		break;
	        	case "3224":
	        		watchType = Vivo4S;
	        		break;
	        	case "3993":
	        		watchType = F255S;
	        		break;
	        	case "3991":
	        		watchType = F255SM;
	        		break;
				default:		
	        }
        }
        
        if (240 == SCREEN_SIZE) {
        	switch (partNumber) {
	        	case "3076":
	        		watchType = F245;
	        		break;
	    		case "3077":
	    			watchType = F245M;
	    			break;
				case "2886":
	    			watchType = F645;
	    			break;
				case "2888":
	    			watchType = F645M;
	    			break;
	    		case "3113":
	    			watchType = F945;
	    			break;	
	    		case "3652":
	    			watchType = F945LTE;
	    			break;
	    		case "3111":
	    			watchType = F5XPlus;
	    			break;	
	    		case "3288":
	    			watchType = F6SPro;
	    			break;	
	    		case "3287":
	    			watchType = F6S;
	    			break;	
	    		case "2604":
	    			watchType = F5X;
	    			break;	
	    		case "2900":
	    			watchType = F5SPlus;
	    			break;	
	    		case "3110":
	    			watchType = F5Plus;
	    			break;
	    		case "2697":
	    			watchType = F5;
	    			break;
	    		case "3589":
	    			watchType = F745;
	    			break;	
	    		case "2691":
	    			watchType = F935;
	    			break;	
	    		case "3905":
	    			watchType = F7S;
	    			break;		
	    			
				default:	
	        }
        } 
              
        if (260 == SCREEN_SIZE) {
        	switch (partNumber) {
	        	case "3289":
	        		watchType = F6;
	        		break;
	    		case "3290":
	    			watchType = F6Pro;
	    			break;
	    		case "3225":
	    			watchType = Vivo4;
	    			break;
	    		case "3992":
	    			watchType = F255;
	    			break;
	    		case "3990":
	    			watchType = F255M;
	    			break;
	    		case "3906":
	    			watchType = F7;
	    			break;
				default:		
	        }
        }
        
        if (280 == SCREEN_SIZE) {
        	switch (partNumber) {
	    		case "3907":
	    			watchType = F7X;
	    			break;
				default:	
					watchType = F6XPro;
	        }
        }
              
        if (360 == SCREEN_SIZE) {
        	switch (partNumber) {
	        	case "3704":
	        		watchType = Venu2S;
	        		break;
				default:		
	        }
        }
              
        if (390 == SCREEN_SIZE) {
        	switch (partNumber) {
	        	case "3226":
	        		watchType = Venu;
	        		break;
				default:		
	        }
        	switch (partNumber) {
	        	case "3740":
	        		watchType = VenuMB;
	        		break;
				default:		
	        }
        }
              
        if (416 == SCREEN_SIZE) {
        	switch (partNumber) {
	        	case "3703":
	        		watchType = Venu2;
	        		break;
				default:		
	        }
        }
    }
    
    function setWatchLayout(watchType) {
    	switch (watchType) {
        	case F245:
        	case F245M:
        	case F945:
        	case F945LTE:
        	case F745:
        		SMALL_VERTICAL_DOWNSET = 16;
	        	BIG_VERTICAL_DOWNSET = 42;
			    BIG_SECOND_VERTICAL_DOWNSET = 99;
			    SMALL_SECOND_VERTICAL_DOWNSET = 167;
	        	BIG_INSET = 15;
			    SMALL_INSET = 33;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 10;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 22;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 37;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 57;
				LABEL_FONT_SPACING = 5;
				SHORT_LABEL_NUM_LETTERS = 2;
    			LONG_LABEL_NUM_LETTERS = 3;
			    
			    break;
			    
			
        	case F6SPro:
        	case F6S:
        	case F7S:
        		SMALL_VERTICAL_DOWNSET = 9;
	        	BIG_VERTICAL_DOWNSET = 38;
			    BIG_SECOND_VERTICAL_DOWNSET = 95;
			    SMALL_SECOND_VERTICAL_DOWNSET = 158;
	        	BIG_INSET = 15;
			    SMALL_INSET = 28;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 15;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 23;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 39;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 57;
				LABEL_FONT_SPACING = 5;
				SHORT_LABEL_NUM_LETTERS = 2;
    			LONG_LABEL_NUM_LETTERS = 4;
    			SUFFER_TEXT_DOWNSET = -2;
				SUFFER_BOX_HEIGHT = 19;
			    
			    break;
        	
			    
			case F645:
			case F645M:
			case F5XPlus:
        	case F5X:
        	case F5SPlus:
        	case F5Plus:
        	case F5:
        	case F935:
				SMALL_VERTICAL_DOWNSET = 22;
	        	BIG_VERTICAL_DOWNSET = 60;
			    BIG_SECOND_VERTICAL_DOWNSET = 119;
			    SMALL_SECOND_VERTICAL_DOWNSET = 178;
	        	BIG_INSET = 15;
			    SMALL_INSET = 35;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 0;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 0;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 40;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 62;
				LABEL_FONT_SPACING = 7;
				SHORT_LABEL_NUM_LETTERS = 2;
    			LONG_LABEL_NUM_LETTERS = 3;
    			SUFFER_TEXT_DOWNSET = -2;
				SUFFER_BOX_HEIGHT = 19;
			    
			    break;
			    
			case F6C:
			case F5S:
				SMALL_VERTICAL_DOWNSET = 15;
	        	BIG_VERTICAL_DOWNSET = 51;
			    BIG_SECOND_VERTICAL_DOWNSET = 108;
			    SMALL_SECOND_VERTICAL_DOWNSET = 166;
	        	BIG_INSET = 15;
			    SMALL_INSET = 35;
			    MEDIUM_FONT = Graphics.FONT_SYSTEM_LARGE;
				//LABEL_FONT = Graphics.FONT_SYSTEM_TINY;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 5;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 0;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 26;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 57;
				LABEL_FONT_SPACING = 7;
				SHORT_LABEL_NUM_LETTERS = 2;
    			LONG_LABEL_NUM_LETTERS = 3;
    			SUFFER_TEXT_DOWNSET = -4;
				SUFFER_BOX_HEIGHT = 17;
			    
			    break;
			    
			case Vivo4S:
			case F255S:
			case F255SM:
				SMALL_VERTICAL_DOWNSET = 20;
	        	BIG_VERTICAL_DOWNSET = 40;
			    BIG_SECOND_VERTICAL_DOWNSET = 92;
			    SMALL_SECOND_VERTICAL_DOWNSET = 158;
	        	BIG_INSET = 15;
			    SMALL_INSET = 35;
			    MEDIUM_FONT = Graphics.FONT_SYSTEM_LARGE;
				//LABEL_FONT = Graphics.FONT_SYSTEM_TINY;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 5;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 0;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 26;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 57;
				LABEL_FONT_SPACING = 7;
				SHORT_LABEL_NUM_LETTERS = 2;
    			LONG_LABEL_NUM_LETTERS = 3;
    			SUFFER_TEXT_DOWNSET = 0;
				SUFFER_BOX_HEIGHT = 17;
				BIG_LABEL_DOWNSET = 22;
			    
			    break;
			    
		    case F6Pro:
		    case F6:
		    case F255:
		    case F255M:
		    case F7:
				SMALL_VERTICAL_DOWNSET = 19;
	        	BIG_VERTICAL_DOWNSET = 43;
			    BIG_SECOND_VERTICAL_DOWNSET = 107;
			    SMALL_SECOND_VERTICAL_DOWNSET = 184;
		        BIG_INSET = 12;
			    SMALL_INSET = 32;
			    MEDIUM_FONT = Graphics.FONT_NUMBER_MILD;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 13;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 24;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 36;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 65;
				LABEL_FONT_SPACING = 6;
				SMALL_LABEL_FONT_DOWNSET = -2;				
				SHORT_LABEL_NUM_LETTERS = 2;
			    
			    break;
			    
		    case Vivo4:
				SMALL_VERTICAL_DOWNSET = 25;
	        	BIG_VERTICAL_DOWNSET = 49;
			    BIG_SECOND_VERTICAL_DOWNSET = 109;
			    SMALL_SECOND_VERTICAL_DOWNSET = 180;
		        BIG_INSET = 12;
			    SMALL_INSET = 32;
			    //MEDIUM_FONT = Graphics.FONT_NUMBER_MILD;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 10;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 23;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 38;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 58;
				LABEL_FONT_SPACING = 6;
				SMALL_LABEL_FONT_DOWNSET = -2;				
				SHORT_LABEL_NUM_LETTERS = 2;
			    
			    break;		   
			    
		    case F6XPro:
		    case F7X:
		    	SMALL_VERTICAL_DOWNSET = 17;
	        	BIG_VERTICAL_DOWNSET = 43;
			    BIG_SECOND_VERTICAL_DOWNSET = 114;
			    SMALL_SECOND_VERTICAL_DOWNSET = 195;
		        BIG_INSET = 14;
			    SMALL_INSET = 38;
			    MEDIUM_FONT = Graphics.FONT_NUMBER_MILD;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 14;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 26;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 38;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 68;
				LABEL_FONT_SPACING = 6;
				SMALL_LABEL_FONT_DOWNSET = -2;				
				SHORT_LABEL_NUM_LETTERS = 2;
			    
			    break;
			    
			case Venu2:
		    	SMALL_VERTICAL_DOWNSET = 28;
	        	BIG_VERTICAL_DOWNSET = 70;
			    BIG_SECOND_VERTICAL_DOWNSET = 165;
			    SMALL_SECOND_VERTICAL_DOWNSET = 275;
		        BIG_INSET = 20;
			    SMALL_INSET = 55;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 24;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 39;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 57;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 96;
				LABEL_FONT_SPACING = 6;
				SMALL_LABEL_FONT_DOWNSET = 5;				
				SHORT_LABEL_NUM_LETTERS = 2;
				SUFFER_BOX_HEIGHT = 38;
				
				break;
			    
			case Venu:
			case VenuMB:
		    	SMALL_VERTICAL_DOWNSET = 24;
	        	BIG_VERTICAL_DOWNSET = 72;
			    BIG_SECOND_VERTICAL_DOWNSET = 172;
			    SMALL_SECOND_VERTICAL_DOWNSET = 282;
		        BIG_INSET = 20;
			    SMALL_INSET = 55;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 13;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 22;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 59;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 103;
				LABEL_FONT_SPACING = 6;
				SMALL_LABEL_FONT_DOWNSET = 0;				
				SHORT_LABEL_NUM_LETTERS = 2;
				SUFFER_BOX_HEIGHT = 38;
				
				break;
			    
			case Venu2S:
		    	SMALL_VERTICAL_DOWNSET = 24;
	        	BIG_VERTICAL_DOWNSET = 66;
			    BIG_SECOND_VERTICAL_DOWNSET = 151;
			    SMALL_SECOND_VERTICAL_DOWNSET = 252;
		        BIG_INSET = 20;
			    SMALL_INSET = 45;
			    SMALL_BOUNDING_BOX_RELATIVE_HEIGHT = 18;
			    BIG_BOUNDING_BOX_RELATIVE_HEIGHT = 33;
    			SMALL_BOUNDING_BOX_RELATIVE_DROP = 57;
    			BIG_BOUNDING_BOX_RELATIVE_DROP = 87;
				LABEL_FONT_SPACING = 5;
				SMALL_LABEL_FONT_DOWNSET = 0;				
				SHORT_LABEL_NUM_LETTERS = 2;
    			LONG_LABEL_NUM_LETTERS = 3;
				SUFFER_BOX_HEIGHT = 38;
				
				break;
			    
		    default:   	
        
        }
    }
        
    function printWatchType() {
    	var display = "";
    	switch (watchType) {
    		case F245:
    			display = "Forerunner 245";
    			break;
    		case F245M:
    			display = "Forerunner 245 Music";
    			break;
    		case F645:
    			display = "Forerunner 645";
    			break;
    		case F645M:
    			display = "Forerunner 645 Music";
    			break;
    		case F6Pro:
    			display = "Fenix 6 Pro";
    			break;
    		case F6XPro:
    			display = "Fenix 6X Pro";
    			break;
    		case F6:
    			display = "Fenix 6";
    			break;
    		case F945:
    			display = "Forerunner 945";
    			break;
    		case F945LTE:
    			display = "Forerunner 945 LTE";
    			break;
    		case F5XPlus:
    			display = "Forerunner 5X Plus";
    			break;
    		case F6SPro:
    			display = "Fenix 6S Pro";
    			break;
    		case F6S:
    			display = "Fenix 6S";
    			break;
    		case F5X:
    			display = "Fenix 5X";
    			break;
    		case F5SPlus:
    			display = "Fenix 5S Plus";
    			break;
    		case F5S:
    			display = "Fenix 5S";
    			break;
    		case F5Plus:
    			display = "Fenix 5 Plus";
    			break;
    		case F5Plus:
    			display = "Fenix 5";
    			break;
    		case F745:
    			display = "Fenix 745";
    			break;
    		case Venu2:
    			display = "Venu 2";
    			break;
    		case Venu2S:
    			display = "Venu 2S";
    			break;
    		case Venu:
    			display = "Venu";
    			break;
    		case VenuMB:
    			display = "Venu Mercedes Benz";
    			break;
    		case Vivo4:
    			display = "Vivoactive 4";
    			break;
    		case Vivo4S:
    			display = "Vivoactive 4S";
    			break;

    		default: 
    			display = "Not listed";
			
    	}
    	
    	System.println(display);
    }


}