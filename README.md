BIG AND MAF-Y
============

This is a data field for Garmin watches that is designed to show information in a really easy to read, big, format. It shows a bunch of fields including MAF, Strava Suffer, and Training Effect. It relies on colour to show overall data, particularly HR/MAF and if you are long sighted you can probably still read the fields and understand the data without glasses.

The six major fields are all configurable, and your Suffer Score and Training Effect are shown at the top and bottom of the screen.

Using Garmin Express you can configure any of the 6 fields to show one of:

* elapsed time
* distance (km or miles)
* pace (metric or statute)
* time of day
* ascent (meters or feet)
* HR
* Average HR
* MAF HR (The MAF field colours the whole screen with your zone colour to make keeping in zone easier)
* % of time spent in MAF
* amount of time spent above MAF

Heart rate zones are provided by your Garmin Express settings, see https://www8.garmin.com/manuals/webhelp/fenix66s6xpro/EN-US/GUID-30C91919-943C-44E9-8048-901AC0881AEA.html

Training Effect is described at https://www.firstbeat.com/en/consumer-products/features/#training-effect

Training Effect colour zones are:
0 - 2 Easy - Black
2+ Maintain Fitness - Green
3+ Improve Fitness - Yellow
4+ Lots Fitter - Blue
5+ Overreaching - Red 

Suffer Score is based on the Strava algorithm.

MAF HR is an HR field that will show Green if you are within +2 / -5 of your Maximum Aerobic Fitness training level as described by Dr Philip Maffetone. It will show Blue if you are within -10 / -6, and red if you are above +2 of your MAF. MAF is calculated as 180 - your age. If you use a MAF data field then the whole screen will turn the color of the MAF field as it changes.

HOW TO USE:
Set your training activity to show one data field on the screen, then set that data field to BIG AND MAF-Y. Use Garmin Express or the Garmin app on your phone to configure the fields that you want to see. 

Sourcecode for the data field is at https://gitlab.com/nz_brian/HiVisRunField

This is what I use on my watch when I'm racing and running. If you try it I hope you find it useful and it makes your runs more fun. If you want to try customising it please extend it and make it your own - you have a free license to do whatever you want with the code. I'd love it if you tell me what you've done.


RELEASE NOTES:
6.0.6 Added Fenix 7, 7S, 7X and Forerunner 255, 255 Music, 255 S, 255 S Music.
6.0.5 Fixed AppName - now shows as Big and MAF-y
6.0.4 Rewrote the layout code to make it easy to add watch models. Added compatibility with a lot of models
6.0.3 Formatting fixes
6.0.2 Put the field condensor back in so that the large fonts can still display wide data
6.0.1 Moved back to the layout with the large fields in the center of the screen
6.0.0 Rewritten for the Fenix 6 Pro, 6x Pro, and the Forerunner 245 for existing users
5.x Versions for the Forerunner 245 Music
4.x Versions for the Fenix 3/5S
3.x and below - was Hi Vis Run Fields for Forerunner 920Xt and Vivoactive